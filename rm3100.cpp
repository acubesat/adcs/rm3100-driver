#include "rm3100.h"

void RM3100::SetSampleRateReg(int rate) {
    uint8_t value;

    if ((rate <= 600) && (rate >= 300))
        value = 0x92;
    else if ((rate < 300) && (rate >= 150))
        value = 0x93;
    else if ((rate < 150) && (rate >= 75))
        value = 0x94;
    else if ((rate < 75) && (rate >= 37))
        value = 0x95;
    else if ((rate < 37) && (rate >= 18))
        value = 0x96;
    else if ((rate < 18) && (rate >= 9))
        value = 0x97;
    else if ((rate < 9) && (rate >= 4))
        value = 0x98;
    else if ((rate < 4) && (rate >= 3))
        value = 0x99;
    else if ((rate < 3) && (rate >= 2))
        value = 0x9A;
    else if ((rate < 2) && (rate >= 1))
        value = 0x9B;   //About 1Hz
    else
        value = 0x9C;   //About 0.6Hz

    RM3100::WriteRegister(RM3100_TMRC_REG, value);
}

bool RM3100::SelfTest() {
    bool XOK, YOK, ZOK, allOK;
    WriteRegister(RM3100_BIST_REG, RM3100_BIST_DATA);
    uint8_t selfTestResult = ReadRegister(RM3100_BIST_REG);
    if (1 << 6 & selfTestResult)
        ZOK = true;
    else
        ZOK = false;

    if (1 << 5 & selfTestResult)
        YOK = true;
    else
        YOK = false;

    if (1 << 4 & selfTestResult)
        XOK = true;
    else
        XOK = false;

    allOK = XOK && YOK && ZOK;

    return allOK;
}

void RM3100::SetupContinuousMode() {
    WriteRegister(RM3100_TMRC_REG, RM3100_TMRC_DEFAULT);
    for (int i = 0; i < 3; ++i) {
        WriteRegister(RM3100_CCXMSB_REG + 2 * i, RM3100_DEFAULT_CYCLES_MSB);
        WriteRegister(RM3100_CCXMSB_REG + 2 * i + 1, RM3100_DEFAULT_CYCLES_LSB);
    }
    WriteRegister(RM3100_CMM_REG, RM3100_FULL_CONTINUOUS_MEASUREMENT);
}

int32_t *RM3100::ContinuousMeasurement() {
    static int32_t processedMeasurement[3] = {0};
    uint8_t rawMeasurement[9] = {0};
    while (true) {
        uint8_t dataReady = ReadRegister(RM3100_STATUS_REG);
        if (dataReady & (1 << 7))
            break;
    }
    for (int i = 0; i < 9; i++) {
        rawMeasurement[i] = ReadRegister(RM3100_MX_REG + i);
    }
    uint32_t intermediate;
    bool isNegative;
    for (int j = 0; j < 3; j++) {
        if ((rawMeasurement[3 * j] & (1 << 7)) == (1 << 7))
            isNegative = true;
        else
            isNegative = false;
        intermediate = (rawMeasurement[3 * j] << 16) | (rawMeasurement[3 * j + 1] << 8) |
                       rawMeasurement[3 * j + 2]; // combination of 3 registers into 1
        if (isNegative) {
            intermediate = (0xFF << 24) | intermediate;  // sign expansion of 2s complement
        }
        processedMeasurement[j] = static_cast<int32_t>(intermediate);
    }
    return processedMeasurement;
}

int32_t *RM3100::SingleMeasurement() {
    static int32_t processedMeasurement[3] = {0};
    uint8_t rawMeasurement[9] = {0};
    WriteRegister(RM3100_CMM_REG, RM3100_POLL_REG);
    WriteRegister(RM3100_POLL_REG, RM3100_FULL_SINGLE_MEASUREMENT);
    while (true) {
        uint8_t dataReady = ReadRegister(RM3100_STATUS_REG);
        if (dataReady & (1 << 7))
            break;
    }
    for (int i = 0; i < 9; i++) {
        rawMeasurement[i] = ReadRegister(RM3100_MX_REG + i);
    }
    uint32_t intermediate;
    bool isNegative;
    for (int j = 0; j < 3; j++) {
        if ((rawMeasurement[3 * j] & (1 << 7)) == (1 << 7))
            isNegative = true;
        else
            isNegative = false;
        intermediate = (rawMeasurement[3 * j] << 16) | (rawMeasurement[3 * j + 1] << 8) |
                       rawMeasurement[3 * j + 2]; // combination of 3 registers into 1
        if (isNegative) {
            intermediate = (0xFF << 24) | intermediate;  // sign expansion of 2s complement
        }
        processedMeasurement[j] = static_cast<int32_t>(intermediate);
    }
    return processedMeasurement;
}

uint8_t RM3100::ReadRegister(uint8_t registerAddress) {
    uint8_t result;
    uint8_t command = registerAddress | 0x80;
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET);
    HAL_SPI_Transmit(this->hspi, &command, 1, HAL_MAX_DELAY);
    HAL_SPI_Receive(this->hspi, &result, 1, HAL_MAX_DELAY);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_SET);
    return result;
}

void RM3100::WriteRegister(uint8_t registerAddress, uint8_t data) {
    uint8_t command[2];
    command[0] = registerAddress;
    command[1] = data;
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET);
    HAL_SPI_Transmit(this->hspi, command, 2, HAL_MAX_DELAY);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_SET);
}

RM3100::RM3100(SPI_HandleTypeDef *hspi) {
    this->hspi = hspi;
};
