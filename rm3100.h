
#include "stm32l4xx_hal.h"

#define RM3100_POLL_REG     0x00 /**< RW; Poll for a single measurement*/
#define RM3100_CMM_REG      0x01 /**< RW; Initiate continuous measurement mode*/
#define RM3100_CCXMSB_REG   0x04 /**< RW; Cycle count X-axis */
#define RM3100_CCXLSB_REG   0x05
#define RM3100_CCYMSB_REG   0x06 /**< RW; Cycle count Y-axis */
#define RM3100_CCYLSB_REG   0x07
#define RM3100_CCZMSB_REG   0x08 /**< RW; Cycle count Z-axis */
#define RM3100_CCZLSB_REG   0x09
#define RM3100_TMRC_REG     0x0B /**< RW; Set data rate in continuous measurement mode*/
#define RM3100_MX_REG       0x24 /**< RW; Measurement Result X-axis, Signed 24-bit */
#define RM3100_BIST_REG     0x33 /**< RW; Built-in self test */
#define RM3100_BIST_DATA    0x8F /** Checks all 3 registers for 4 sleep oscillation cycles and 4 LR periods  */
#define RM3100_STATUS_REG   0x34 /**< R; Status of DRDY */
#define RM3100_REVID_REG    0x36 /**< R; Revision ID, default 0x22 */
#define RM3100_FULL_SINGLE_MEASUREMENT 0x70 /** When this byte is written to the poll register(0x00), the measurement returns all 3 axes. */
#define RM3100_FULL_CONTINUOUS_MEASUREMENT 0x71 /** When this byte is written to the CMM register(0x01), the measurement returns all 3 axes. */
#define RM3100_TMRC_DEFAULT 0x96 /** This is the default setting for the sampling rate register (~37 Hz) */
#define RM3100_DEFAULT_CYCLES_MSB 0x00 /** Default cycles for the MSB registers */
#define RM3100_DEFAULT_CYCLES_LSB 0xC8 /** Default cycles for the LSB registers */

/**
 * This is the main class of the RM3100 driver
 */

class RM3100 {

public:

    /**
     * This function changes the sampling rate of the magnetometer.
     * @param rate Sampling rate of the magnetometer
     * @param hspi SPI protocol handle
     */

    void SetSampleRateReg(int rate);

    /**
     * This function writes the specified data to the named register.
     * The data can consist of multiple bytes.
     * @param hspi SPI protocol handle
     * @param registerAddress Address of the register to be written
     * @param data Content to be written to the register
     */
    void WriteRegister(uint8_t registerAddress, uint8_t data);

    /**
     * This function reads from the specified register.
     * @param hspi SPI protocol handle
     * @param registerAddress Address of the register to be read
     * @return Content of the register that was read
     */
    uint8_t ReadRegister(uint8_t registerAddress);

    /**
     * This function makes a single (polling) measurement.
     * @param hspi SPI protocol handle
     * @return Pointer to the 3D array containing the 3-axis magnetometer measurement
     */
    int32_t *SingleMeasurement();

    /**
     * This function reads from the magnetometer while in continuous measurement mode.
     * Continuous mode must have already been initiated by running the SetupContinuousMode function.
     * @param hspi SPI protocol handle
     * @return Pointer to the 3D array containing the 3-axis magnetometer measurement
     */
    int32_t *ContinuousMeasurement();

    /**
     * This function initializes the continuous measurement mode with the default cycles and sampling rate.
     * @param hspi SPI protocol handle
     */
    void SetupContinuousMode();

    /**
     * This function runs the self test procedure using the following settings:
     * Checks all 3 registers for 4 sleep oscillation cycles and 4 LR periods
     * @param hspi SPI protocol handle
     * @return Whether the tests on all axes were successful.
     */
    bool SelfTest();

    RM3100(SPI_HandleTypeDef *hspi);

private:
    SPI_HandleTypeDef *hspi;
};