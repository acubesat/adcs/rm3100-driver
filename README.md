# RM3100 SPI Driver

This contains the template functions needed for operating the RM3100 magnetometer using the SPI protocol.

Said functions were written with STM32 MCUs in mind. To adapt them for use with another microcontroller, 2 changes are required.

1. The SPI handle passed to the RM3100 constructor must be appropriate to the platform you are trying to run it on.
2. The WriteRegister and ReadRegister functions must be modified to use the platform-appropriate SPI receive and transmit functions, as well as the chosen chip-select pin.

## Disclaimers:

1. The code included here is blocking, not interrupt-based.
2. The measurements acquired from the driver are the raw magnetometer output, which has a range (-8388608 to 8388607). If you are interested in the absolute magnitude of each axis, they must first be scaled. The RM3100 datasheet reports a Field Measurement Range of -800 to 800 uT. 
